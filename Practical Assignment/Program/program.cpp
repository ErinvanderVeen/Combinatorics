#include "program.h"

void PrintVector(std::vector<unsigned int>& v) {
	for (unsigned int i = 0; i < v.size(); i++) {
		std::cout << " | " << v.at(i);
	}
	std::cout << " |" << std::endl;
}

// Uses DFS to print all possible color arrangements
void PrintWays(unsigned int n, unsigned int m) {
	std::vector<unsigned int> v (1, 1);
	while (true) {
		if (v.back() > m) {
			v.pop_back();
			if (v.empty())
				break;
			v.back()++;
		} else if (v.size() >= 2 && v.back() == v.at(v.size() - 2)) {
			v.back()++;
		} else if (v.size() == n) {
			if (v.front() != v.back() || v.size() == 1) {
				PrintVector(v);
			}
			v.back()++;
		} else {
			v.push_back(1);
		}
	}
}

// Calculates nCr
long long Combination(unsigned int n, unsigned int r)
{
	// We cannot pick more than n from a set
	// if that sets holds only n elements
	if (r > n)
		return 0;

	// We know that nCr = nCn-r
	// We use this to reduce the amount we have to calculate
	if (r * 2 > n)
		r = n-r;

	// There is only 1 way to pick 0 from n
	if (r == 0)
		return 1;

	// Actually start the calculation using the formula
	//
	//    n!
	// --------
	// r!(n-r)!
	//
	long long result = n;
	for (long long i = 2; i <= r; ++i) {
		result *= (n-i+1);	// Top part of formula
		result /= i;		// Bottom part of formula
	}
	return result;
}

// Using principle of inclusion exclusion
// We return N(p1'p2'p3'...) where
// p1 means that wall 1 has the same color as wall n
// p2 means that wall 2 has the same color as wall 1
// p3 means that wall 3 has the same color as wall 2
// ...
long long CalculateNumWays(unsigned int n, unsigned int m) {
	long long num_ways = 0;

	// Every property is identical to another, therefore
	// it is not necessary to know which properties we are testing
	// Only the amount of properties, and how often this amount of
	// properties needs to be tested
	for (unsigned int i = 0; i < n; i++) {

		// If the number of properties is divisible by 2, we need to add,
		// otherwise we need to subtract. This is given by the
		// college slides
		num_ways += pow(-1, i) * Combination(n, i) * pow(m, n - i);
	}

	// Edge case where there is only one wall.
	// There are no properties that can be used
	if (n != 1)
		num_ways += pow(-1, n) * pow(m, 1);

	// Print and return the result
	std::cout 	<< "There are/is " << num_ways
				<< " way(s) to paint " << n 
				<< " wall(s) with " << m 
				<< " color(s)." << std::endl;
	return num_ways;
}

main(int argc, char* argv[]) {
	// n = number of walls
	// m = number of paint colors
	unsigned int n = atoi(argv[1]), m = atoi(argv[2]);

	// If there are 0 ways to paint the walls,
	// there is no point in printing the ways
	// -p indicates the ways need to be printed
	if (CalculateNumWays(n, m) > 0 && argc >= 4 && strcmp(argv[3],"-p") == 0) {
		PrintWays(n, m);
	}

	return 0;
}

