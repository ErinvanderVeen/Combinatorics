#ifndef PROGRAM_H
#define PROGRAM_H

#include <vector>
#include <iostream>
#include <math.h>
#include <algorithm>
#include <string.h>

void PrintVector(std::vector<unsigned int>&);
void PrintWays(unsigned int, unsigned int);
long long Combination(unsigned int, unsigned int);
long long CalculateNumWays(unsigned int, unsigned int);

#endif

